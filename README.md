# C-MORE brain MRI analysis pipeline

Code for the analysis pipeline presented in the paper
> ["Adapting the UK Biobank brain imaging protocol and analysis pipeline for the C-MORE multi-organ study of COVID-19 survivors"](https://www.medrxiv.org/content/10.1101/2021.05.19.21257316v1)

Used in the project:
> "Capturing MultiORgan Effects of COVID-19’ (C-MORE) study". Details of the main multiorgan study are described [here](https://www.thelancet.com/journals/eclinm/article/PIIS2589-5370(20)30427-2/fulltext)

## How to cite
Our preprint is available [here](https://www.medrxiv.org/content/10.1101/2021.05.19.21257316v1)

## Contents
* **T1-weighted.**
This sequence was exactly matched to the one used in UKB and images were processed using the [UKB pipeline](https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1)
*  **T2-FLAIR.** 
This sequence was closely matched to the one used in UKB.
Images were processed using the [UKB pipeline](https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1) to extract white matter hyperintensities (WMH).
Additional IDPs extracted in this study:
    * Size and FLAIR intensity of the olfactory bulb (OB) - see `OB_pipeline` for details
    * Periventricular WMH (PWMH) and deep WMH (DWMH) volumes using the `bianca_perivent_deep` script
* **Diffusion weighted MRI (dMRI).**
In this multiorgan protocol only 3 orthogonal diffusion directions were acquired. The [UKB pipeline](https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1) was therefore adapted for preprocessing the images and derive mean diffusivity (MD) IDPs. Scripts are in the `dMRI_pipeline` folder.  Preprocessing was performed using the `cmore_diff.sh` script. Average MD was then calculated within an average skeleton in standard space for each tract of the JHU atlas using the `cmore_tbss_md` script (modified from bb_tbss of the UKB pipeline) and also within the normal appearing white matter only (i.e. outside WMH) using the `cmore_tbss_md_NAWM` script.
* **Susceptibility-weighted MR Imaging (SWI).**
This sequence was closely matched to the one used in UKB.
Images were processed using the [UKB pipeline](https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1) to extract T2* in 14 structures.
Additional IDPs extracted in this study are quantitative susceptibility mapping (QSM) values, referenced to the CSF QSM value, from 14 structures. See `QSM_pipeline` for details
* **Arterial Spin Labelling (ASL).**
This sequence was not included in UKB at the time of this study. See `ASL_pipeline` for details on preprocessing pipeline and IDP extraction.

## Useful resources
[UK Biobank Brain Imaging Documentation](https://biobank.ndph.ox.ac.uk/showcase/showcase/docs/brain_mri.pdf)
