# Olfactory bulb (OB) analysis pipeline
Main scripts:
1) `cmore_run_reg2template.sh`: registers individual images to the template using MMORF and warps masks from template space back to individuals' native spaces
2) `cmore_segSize.py`: calculates the volume of the masks in individuals' native spaces
3) `cmore_segIntensity.sh`: calculates the T2-FLAIR intensity (raw and normalised for WM) within the OB masks in individuals' native spaces

MMORF: https://git.fmrib.ox.ac.uk/flange/mmorf_beta 

Support files:
- Template: `unbiased_nonlinear_template_it18_T2_head.nii.gz`
- Olfactory bulb labels mask: `unbiased_nonlinear_template_it18_ob_labels_bin.nii.gz`

