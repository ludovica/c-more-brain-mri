#!/bin/bash

# Author: Christoph Arthofer
# Copyright: FMRIB 2021

#$ -cwd -j y
#$ -r y

echo `date`: Executing task ${SGE_TASK_ID} of job ${JOB_ID} on `hostname` as user ${USER}
echo SGE_TASK_FIRST=${SGE_TASK_FIRST}, SGE_TASK_LAST=${SGE_TASK_LAST}, SGE_TASK_STEPSIZE=${SGE_TASK_STEPSIZE}

. ~/.bash_profile
. $MODULESHOME/init/bash
module load fsl/6.0.3

ID_PATH=$1
DATA_DIR=$2
TEMP_DIR=$3
OUT_DIR=$4

IMG_IN_ID=$(awk 'NR=='${SGE_TASK_ID} ${ID_PATH})

IMG_IN_PATH="${DATA_DIR}${IMG_IN_ID}/T1/T1_unbiased_brain.nii.gz"
IMG_REF_PATH="${TEMP_DIR}T1_template_brain.nii.gz"
T1_OMAT_OUT_PATH="${OUT_DIR}${IMG_IN_ID}_to_template_T1reg.mat"

echo "flirt -in ${IMG_IN_PATH} -ref ${IMG_REF_PATH} -omat ${T1_OMAT_OUT_PATH} -dof 12"
flirt -in ${IMG_IN_PATH} -ref ${IMG_REF_PATH} -omat ${T1_OMAT_OUT_PATH} -dof 12



IMG_IN_PATH="${DATA_DIR}${IMG_IN_ID}/T2_FLAIR/T2_FLAIR_unbiased_brain.nii.gz"
IMG_REF_PATH="${DATA_DIR}${IMG_IN_ID}/T1/T1_unbiased_brain.nii.gz"
T2_to_T1_OMAT_OUT_PATH="${OUT_DIR}${IMG_IN_ID}_T2_to_T1.mat"

echo "flirt -in ${IMG_IN_PATH} -ref ${IMG_REF_PATH} -omat ${T2_to_T1_OMAT_OUT_PATH} -dof 6"
flirt -in ${IMG_IN_PATH} -ref ${IMG_REF_PATH} -omat ${T2_to_T1_OMAT_OUT_PATH} -dof 6



T2_OMAT_OUT_PATH="${OUT_DIR}${IMG_IN_ID}_to_template_T2reg.mat"
echo "convert_xfm -omat ${T2_OMAT_OUT_PATH} -concat ${T1_OMAT_OUT_PATH} ${T2_to_T1_OMAT_OUT_PATH}"

convert_xfm -omat ${T2_OMAT_OUT_PATH} -concat ${T1_OMAT_OUT_PATH} ${T2_to_T1_OMAT_OUT_PATH}

rv=$?

echo `date`: task complete
exit $rv