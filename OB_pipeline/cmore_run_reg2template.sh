#!/bin/bash

# Author: Christoph Arthofer
# Copyright: FMRIB 2021


TAG='cmore'
BASE_DIR="cmore/data/output/"
DATA_DIR="cmore/data/orig/"
ID_PATH="cmore/data/IDs.csv"
TEMP_DIR="cmore/data/template/"
PARAM_IT_PATH="cmore/data/mmorf_params_it18.ini"

N_IMGS=$(cat ${ID_PATH} | wc -l)
i_outp=0

# Create output directory for affine registrations
AFFINE_DIR="${BASE_DIR}affine_it1/"
mkdir -p ${AFFINE_DIR}

# Register all individual images to one reference image
i_outp=$((i_outp+1))
printf -v i "%04d" $i_outp
qsub -o ${BASE_DIR}${i}_output_affine_FLIRT_testing.log \
  -N ${TAG}_registrations_affine_testing_it1 \
  -q short.qc \
  -t 1-${N_IMGS}:1 \
  cmore_FLIRT_registration.sh ${ID_PATH} ${DATA_DIR} ${TEMP_DIR} ${AFFINE_DIR}

# ----------------------------------------------------------------------------------------------------------------------
# Nonlinear registrations
NLN_DIR="${BASE_DIR}nln_testing_it1/"
mkdir -p ${NLN_DIR}

i_outp=$((i_outp+1))
printf -v i "%04d" $i_outp
REF_DIR=${NLN_REF_DIR}
qsub -hold_jid ${TAG}_registrations_affine_testing_it1 \
  -o ${BASE_DIR}${i}_output_nln_registrations_it1.log \
  -N ${TAG}_registrations_mmorf \
  -q gpu8.q \
  -l gpu=1 \
  -l gputype=p100 \
  -t 1-${N_IMGS}:1 \
  cmore_mmorf.sh ${ID_PATH} ${TEMP_DIR} ${DATA_DIR} ${AFFINE_DIR} ${NLN_DIR} ${PARAM_IT_PATH}

i_outp=$((i_outp+1))
printf -v i "%04d" $i_outp
qsub -hold_jid ${TAG}_registrations_mmorf \
  -o ${BASE_DIR}${i}_output_cmore_inverseWarps.log \
  -N ${TAG}_invertWarps \
  -q short.qc \
  -t 1-${N_IMGS}:1 \
  cmore_invertWarps.sh ${ID_PATH} ${DATA_DIR} ${NLN_DIR} ${TEMP_DIR}
