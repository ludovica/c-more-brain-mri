#!/usr/bin/env python

# Author: Christoph Arthofer
# Copyright: FMRIB 2021

import nibabel as nib
import numpy as np
import pandas as pd

id_paths = ['IDs.txt']
label_path_temp = "/path/to/labels/OB_labels_to_%s.nii.gz"

vols = []
ids = []
tbvs = []
for id_path in id_paths:
    df_ids = pd.read_csv(id_path, header=None, names=['subject_ID'], dtype={'subject_ID':str})
    ls_ids = df_ids['subject_ID'].tolist()

    for id in ls_ids:
        img_path = label_path_temp % id
        label_nib = nib.load(img_path)
        header = label_nib.header
        label_arr = label_nib.get_fdata()
        mask_count = np.sum(label_arr)
        vol = mask_count * np.prod(header['pixdim'][1:4])
        print('id:', id, ' pixdim:', header['pixdim'], ' volume:', vol)
        vols.append(vol)
        ids.append(id)

ob_vols = {'vol':vols}
df = pd.DataFrame(data=ob_vols,index=ids)
print(df)
df.to_csv('/path/to/volume/measurement-output/OB_volumes.csv',header=True)
