#!/bin/bash

# Author: Christoph Arthofer
# Copyright: FMRIB 2021

#$ -cwd -j y
#$ -r y

export SINGULARITY_BIND="/"

echo `date`: Executing task ${SGE_TASK_ID} of job ${JOB_ID} on `hostname` as user ${USER}
echo SGE_TASK_FIRST=${SGE_TASK_FIRST}, SGE_TASK_LAST=${SGE_TASK_LAST}, SGE_TASK_STEPSIZE=${SGE_TASK_STEPSIZE}

. ~/.bash_profile
. $MODULESHOME/init/bash
module load fsl/6.0.3

ID_PATH=$1
IMG_IN_ID=$(awk 'NR=='${SGE_TASK_ID} ${ID_PATH})

REF_DIR=$2
IMG_T1_HEAD_REF_PATH="${REF_DIR}T1_template_head.nii.gz"
IMG_T1_BRAIN_REF_PATH="${REF_DIR}T1_template_brain.nii.gz"
IMG_T2_HEAD_REF_PATH="${REF_DIR}T2_template_head.nii.gz"

DATA_DIR=$3
IMG_T1_HEAD_IN_PATH="${DATA_DIR}${IMG_IN_ID}/T1/T1_unbiased.nii.gz"
IMG_T1_BRAIN_IN_PATH="${DATA_DIR}${IMG_IN_ID}/T1/T1_unbiased_brain.nii.gz"
IMG_T2_HEAD_IN_PATH="${DATA_DIR}${IMG_IN_ID}/T2_FLAIR/T2_FLAIR_unbiased.nii.gz"

AFFINE_DIR=$4
OMAT_T1_IN_PATH="${AFFINE_DIR}${IMG_IN_ID}_to_template_T1reg.mat"
OMAT_T2_IN_PATH="${AFFINE_DIR}${IMG_IN_ID}_to_template_T2reg.mat"
UNITY_REF_PATH="ident.mat"

NLN_DIR=$5
WARP_OUT="${NLN_DIR}${IMG_IN_ID}_to_template_warp.nii.gz"
JAC_OUT="${NLN_DIR}${IMG_IN_ID}_to_template_jac.nii.gz"
BIAS_OUT="${NLN_DIR}${IMG_IN_ID}_to_template_bias"
WARP_COMP_PATH="${NLN_DIR}${IMG_IN_ID}_to_template_warp_T2_composed.nii.gz"

PARAM_IT_PATH=$6

singularity run --nv /path/to/mmorf.sif \
--config ${PARAM_IT_PATH} \
--img_warp_space ${IMG_T1_HEAD_REF_PATH} \
--img_ref_scalar ${IMG_T1_HEAD_REF_PATH} \
--img_mov_scalar ${IMG_T1_HEAD_IN_PATH} \
--aff_ref_scalar ${UNITY_REF_PATH} \
--aff_mov_scalar ${OMAT_T1_IN_PATH} \
--mask_ref_scalar NULL \
--mask_mov_scalar NULL \
--img_ref_scalar ${IMG_T1_BRAIN_REF_PATH} \
--img_mov_scalar ${IMG_T1_BRAIN_IN_PATH} \
--aff_ref_scalar ${UNITY_REF_PATH} \
--aff_mov_scalar ${OMAT_T1_IN_PATH} \
--mask_ref_scalar NULL \
--mask_mov_scalar NULL \
--img_ref_scalar ${IMG_T2_HEAD_REF_PATH} \
--img_mov_scalar ${IMG_T2_HEAD_IN_PATH} \
--aff_ref_scalar ${UNITY_REF_PATH} \
--aff_mov_scalar ${OMAT_T2_IN_PATH} \
--mask_ref_scalar NULL \
--mask_mov_scalar NULL \
--warp_out ${WARP_OUT} \
--jac_det_out ${JAC_OUT} \
--bias_out ${BIAS_OUT}

convertwarp --ref=${IMG_T2_HEAD_REF_PATH} --premat=${OMAT_T2_IN_PATH} --warp1=${WARP_OUT} --out=${WARP_COMP_PATH}

rv=$?

echo `date`: task complete
exit $rv
