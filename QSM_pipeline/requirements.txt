MATLAB R2017b or higher
STI Suite - https://people.eecs.berkeley.edu/~chunlei.liu/software.html
FSL - https://fsl.fmrib.ox.ac.uk/fsldownloads_registration
Mixture Modelling tool - https://github.com/allera/One_Dim_Mixture_Models
