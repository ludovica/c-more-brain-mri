#!/bin/bash
#
# Script name: bianca_perivent_deep
#
# Description: Script to calculate volume of total, periventricular and deep white matter hyperintensities
#
# Authors: Ludovica Griffanti, Mark Jenkinson
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
set -e
# set -x

if [ $# -lt 5 ] ; then
  echo "Usage:  `basename $0` <thresholded_binarised_WMH_map> <ventricles_mask> <minclustersize> <do_stats 0 1 2> <outputdir>"
  echo " This script separates WMH into periventricular and deep WMH, saves two separate binary images (perivent_map and deepwm_map) and calculates volume of total and separate WMHs"
  echo " Criterion = 10mm rule: a lesion within 10 mm (included) from the ventricles is classified as periventricular, otherwise as deep."
  echo "      INPUT 1: <thresholded_binarised_WMH_map>"
  echo " This script was created as postprocessing after WMH segmentation with BIANCA. The input file requested is BIANCA output thresholded at the desired threshold and binarised,  e.g. running:"
  echo "fslmaths BIANCA_output -thr 0.9 -bin BIANCA_out_thr09_bin"
  echo "and using BIANCA_out_thr09_bin as input for this script"
  echo "      INPUT 2: <ventricles_mask>"
  echo " If make_bianca_mask.sh was used to create an exlusion mask for BIANCA output, the ventricle mask to use is the file ventmask.nii.gz"
  echo " If T1 and FLAIR were not in the same space, ventricle mask needs to be registered to FLAIR (and binarised)"
  echo "      INPUT 3: minimum cluster (volume) size (in voxels) to consider"
  echo " Use 0 for no cluster thresholding"
  echo "      INPUT 4: <do_stats 0 1 2>"
  echo "if 0 it will only produce the images and not calculate volumes"
  echo "if 1 it will calculate volumes for total, periventricular and deep WMH and display on the screen"
  echo "if 2 it will calculate volumes and save them in the file WMH_tot_pvent_deep_10mm.txt"
  exit 0
fi

bianca_out_bin=$1
ventricle_mask=$2
minclustersize=$3
do_stats=$4
outdir=$5

outputdir=${outdir}/pwmh_dwmh_output
if [ ! -d $outputdir ]; then
    mkdir $outputdir
fi

# Cluster thresholding if needed
if [ ${minclustersize} -gt 0 ]; then
# create indexed version of all the lesions in the mask (above minclustersize)
cluster --in=$bianca_out_bin --thresh=0.05 --oindex=$outputdir/bianca_out_mincluster${minclustersize} --connectivity=6 --no_table --minextent=${minclustersize}
fslmaths $outputdir/bianca_out_mincluster${minclustersize} -bin $outputdir/bianca_out_mincluster${minclustersize}
bianca_out_bin=$outputdir/bianca_out_mincluster${minclustersize}
fi

# CRITERION: Distance from ventricles (10 mm)
distancemap -i $ventricle_mask -m $bianca_out_bin -o $outputdir/bianca_out_bin_distvent
# If a lesion is within 10 mm (included) from the ventricles is periventricular, if not, it is deep.
# generating deep mask first by taking everything above 10.0001 (fslmaths -thr is inclusive) and generating pvent by subtraction from the original WMH.

fslmaths ${outputdir}/bianca_out_bin_distvent -thr 10.0001 -bin ${outputdir}/deepwm_map_10mm
fslmaths $bianca_out_bin -sub ${outputdir}/deepwm_map_10mm $outputdir/perivent_map_10mm

# volumes extraction
if [ $do_stats -gt 0 ]  ; then
    tot_vol=`fslstats $bianca_out_bin -V | awk '{print $2}'`
    pvent_vol_10mm=`fslstats $outputdir/perivent_map_10mm -V | awk '{print $2}'`
    deep_vol_10mm=`fslstats $outputdir/deepwm_map_10mm -V | awk '{print $2}'`

    if [ "$do_stats" == "2" ] ; then
	echo $tot_vol $pvent_vol_10mm $deep_vol_10mm >>  $outputdir/WMH_tot_pvent_deep_10mm.txt
    elif [ "$do_stats" == "1" ] ; then
	echo "tot_volume= $tot_vol DISTANCE10mm:pvent_vol_10mm= $pvent_vol_10mm deep_vol_10mm= $deep_vol_10mm "
    fi 
fi

