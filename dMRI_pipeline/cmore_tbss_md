#!/bin/bash
#
# Script name: cmore_tbss_md
#
# Description: Script to calculate MD values from WM tracts
#  (adapted pipeline from dMRI sequence with 3 diffusion directions)
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith & Mark Jenkinson
#
# Copyright 2020 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
. "$BB_BIN_DIR"/bb_pipeline_tools/bb_set_header 

direc="$PWD"/"$1"
TBSSDir="$direc"/dMRI/TBSS
mkdir "$TBSSDir"
cd "$TBSSDir"
mkdir -p FA

#################
######  1  ######
#################
f=dti_FA

ln -s $PWD/../dMRI/dtifit_output/$f.nii.gz $f.nii.gz

echo processing $f

# erode a little and zero end slices
X=`${FSLDIR}/bin/fslval $f dim1`; X=`echo "$X 2 - p" | dc -`
Y=`${FSLDIR}/bin/fslval $f dim2`; Y=`echo "$Y 2 - p" | dc -`
Z=`${FSLDIR}/bin/fslval $f dim3`; Z=`echo "$Z 2 - p" | dc -`
$FSLDIR/bin/fslmaths $f -min 1 -ero -roi 1 $X 1 $Y 1 $Z 0 1 FA/${f}

# create mask (for use in FLIRT & FNIRT)
$FSLDIR/bin/fslmaths FA/${f} -bin FA/${f}_mask
$FSLDIR/bin/fslmaths FA/${f}_mask -dilD -dilD -sub 1 -abs -add FA/${f}_mask FA/${f}_mask -odt char

cd FA


#################
######  2  ######
#################
# now estimate the mean deformation
${FSLDIR}/bin/fslmaths ../../../T1/transforms/T1_to_MNI_warp.nii.gz -sqr -Tmean warp_tmp
${FSLDIR}/bin/fslstats warp_tmp -M -P 50 > warp.msf
${FSLDIR}/bin/imrm warp_tmp


#################
######  3  ######
#################
best=MNI
echo "$best" > best.msf
mkdir -p ../stats

applywarp -i ../../dMRI/dtifit_output/dti_FA.nii.gz -o dti_FA_to_MNI -r /opt/fmrib/fsl/data/standard/MNI152_T1_1mm.nii.gz --premat=../../dMRI/hifib0_ud_to_T1.mat -w ../../../T1/transforms/T1_to_MNI_warp.nii.gz --interp=spline
applywarp -i ../../dMRI/dtifit_output/dti_MD.nii.gz -o dti_MD_to_MNI -r /opt/fmrib/fsl/data/standard/MNI152_T1_1mm.nii.gz --premat=../../dMRI/hifib0_ud_to_T1.mat -w ../../../T1/transforms/T1_to_MNI_warp.nii.gz --interp=spline

cd ../stats 
"$FSLDIR"/bin/imcp ../FA/dti_FA_to_MNI all_FA

# create mean FA
"$FSLDIR"/bin/fslmaths all_FA -bin -mul "$FSLDIR"/data/standard/FMRIB58_FA_1mm -bin mean_FA_mask
"$FSLDIR"/bin/fslmaths all_FA -mas mean_FA_mask all_FA
"$FSLDIR"/bin/fslmaths "$FSLDIR"/data/standard/FMRIB58_FA_1mm -mas mean_FA_mask mean_FA
"$FSLDIR"/bin/fslmaths "$FSLDIR"/data/standard/FMRIB58_FA-skeleton_1mm -mas mean_FA_mask mean_FA_skeleton


#################
######  4  ######
#################
thresh="2000"
echo "$thresh" > thresh.txt

"$FSLDIR"/bin/fslmaths mean_FA_skeleton -thr "$thresh" -bin mean_FA_skeleton_mask
"$FSLDIR"/bin/fslmaths all_FA -mas mean_FA_skeleton_mask all_FA_skeletonised
"$FSLDIR"/bin/fslstats -K "$FSLDIR"/data/atlases/JHU/JHU-ICBM-labels-1mm all_FA_skeletonised.nii.gz -M >JHUrois_FA.txt


#################
######  5  ######
#################
"$FSLDIR"/bin/imcp ../FA/dti_MD_to_MNI all_MD
"$FSLDIR"/bin/fslmaths all_MD -mas mean_FA_skeleton_mask all_MD_skeletonised
"$FSLDIR"/bin/fslstats -K "$FSLDIR"/data/atlases/JHU/JHU-ICBM-labels-1mm all_MD_skeletonised.nii.gz -M >JHUrois_MD.txt

cd ../..

#################
######  6  ######
#################


#Setting the string of NaN in case there is a problem.
numVars="48"
nanResult="";
for i in $(seq 1 $numVars) ; do 
    nanResult="NaN $nanResult" ; 
done

result="" 

#for i in FA MD ; do
for i in MD ; do
    if [ -f TBSS/stats/JHUrois_${i}.txt ] ; then
        if [ `cat TBSS/stats/JHUrois_${i}.txt | wc -w` = $numVars ] ; then
            miniResult=`cat TBSS/stats/JHUrois_${i}.txt`
        else
            miniResult="$nanResult"
        fi
    else
        miniResult="$nanResult"
    fi
    result="$result $miniResult"
done

echo $result > ../IDP_files/bb_IDP_diff_TBSS.txt

. "$BB_BIN_DIR"/bb_pipeline_tools/bb_set_footer 

