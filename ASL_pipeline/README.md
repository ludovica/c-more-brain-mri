# ASL analysis pipeline
Order of scripts:
1) `bb_asl.sh`: ASL preprocessing using oxasl with structural and distortion correction
2) `bb_asl_visual_check.sh` : Script to visually inspect the output of the ASL processing pipeline
3) `bb_asl_summary_measures.sh`: extracts summary CBF and ATT metrics in GM and WM
4) `bb_IDP_asl.sh`: Script to generate the IDPs related to ASL and save them in a similar format to the other UK Biobank IDPs
