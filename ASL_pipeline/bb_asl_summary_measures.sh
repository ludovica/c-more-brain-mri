#!/bin/sh
#
# Script name: bb_asl_summary_measures
#
# Description: Script to generate more robust GM/WM masks and extract summary measures from ASL data
#
# Authors: Thomas Okell, Flora Kennedy McConnell, Ludovica Griffanti, Fidel Alfaro-Almagro
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header 

origDir=`pwd`
direc=$1

cd $direc/ASL/PerfQuantFull/native_space

# Generate the mask: threshold at 50% and mask by brain mask also. WM same but threshold 0.8
$FSLDIR/bin/fslmaths pvgm_inasl.nii.gz -mul mask.nii.gz -thr 0.5 -bin gm_mask_0.5
$FSLDIR/bin/fslmaths pvwm_inasl.nii.gz -mul mask.nii.gz -thr 0.8 -bin wm_mask_0.8

# Generate summary CBF and arrival metrics
MEANGMCBF=`fslstats perfusion_calib -k gm_mask_0.5.nii.gz -m`
echo "Mean GM CBF\: $MEANGMCBF"
echo "$MEANGMCBF" > ../../CBF_gm_mean.txt
MEANGMATT=`fslstats arrival -k gm_mask_0.5.nii.gz -m`
echo "Mean GM ATT\: $MEANGMATT"
echo "$MEANGMATT" > ../../ATT_gm_mean.txt
# WM
MEANWMCBF=`fslstats perfusion_calib -k wm_mask_0.8.nii.gz -m`
echo "Mean WM CBF\: $MEANWMCBF"
echo "$MEANWMCBF" > ../../CBF_wm_mean.txt
MEANWMATT=`fslstats arrival -k wm_mask_0.8.nii.gz -m`
echo "Mean WM ATT\: $MEANWMATT"
echo "$MEANWMATT" > ../../ATT_wm_mean.txt

# Generate pvcorr summary CBF and arrival metrics
cd pvcorr
MEANGMCBFpvcorr=`fslstats perfusion_calib -k ../gm_mask_0.5.nii.gz -m`
echo "Mean GM CBF PVCORR\: $MEANGMCBFpvcorr"
echo "$MEANGMCBFpvcorr" > ../../../CBF_pvcorr_gm_mean.txt
MEANGMATTpvcorr=`fslstats arrival -k ../gm_mask_0.5.nii.gz -m`
echo "Mean GM ATT PVCORR \: $MEANGMATTpvcorr"
echo "$MEANGMATTpvcorr" > ../../../ATT_pvcorr_gm_mean.txt
# WM
MEANWMCBFpvcorr=`fslstats perfusion_wm_calib -k ../wm_mask_0.8.nii.gz -m`
echo "Mean WM CBF PVCORR\: $MEANWMCBFpvcorr"
echo "$MEANWMCBFpvcorr" > ../../../CBF_pvcorr_wm_mean.txt
MEANWMATTpvcorr=`fslstats arrival_wm -k ../wm_mask_0.8.nii.gz -m`
echo "Mean WM ATT PVCORR \: $MEANWMATTpvcorr"
echo "$MEANWMATTpvcorr" > ../../../ATT_pvcorr_wm_mean.txt

cd $origDir

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_footer 