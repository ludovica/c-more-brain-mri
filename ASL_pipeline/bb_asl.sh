#!/bin/sh
#
# Script name: bb_asl
#
# Description: Script to process ASL data with oxford_asl
#
# Authors: Thomas Okell, Flora Kennedy McConnell, Ludovica Griffanti, Fidel Alfaro-Almagro
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header

set -x

dirScript=`dirname $0`

export FSLDIR="/opt/fmrib/fsl/"
export FSLCONFDIR=${FSLDIR}/config
export FSLOUTPUTTYPE="NIFTI_GZ"
export PATH="$FSLDIR/bin:$PATH"

echo ">>>>>> Running $0..."

subject="$1"

# Find the relevant files
ASL=$subject/ASL/ASL.nii.gz
DIFFCORR=$subject/dMRI/dMRI/TOPUP_iout.nii.gz
DIFFFMAP=$subject/dMRI/dMRI/TOPUP_fout.nii.gz
STRUC=$subject/T1/T1_unbiased.nii.gz
STRUCBRAIN=$subject/T1/T1_unbiased_brain.nii.gz
FASTSRC=$subject/T1/T1_fast/T1_brain
WARP=$subject/T1/transforms/T1_to_MNI_warp_coef.nii.gz
MNI=$FSLDIR/data/standard/MNI152_T1_1mm.nii.gz

ASL=`remove_ext $ASL`
DIFFCORR=`remove_ext $DIFFCORR`
DIFFFMAP=`remove_ext $DIFFFMAP`
STRUC=`remove_ext $STRUC`
WARP=`remove_ext $WARP`

echo Found files to use:
echo "ASL:                 $ASL"
echo "Diffusion corrected: $DIFFCORR"
echo "Diffusion fieldmap:  $DIFFFMAP"
echo "Structural:          $STRUC"

# Split up the ASL data into M0 and main ASL data
rm -rf $subject/ASL/ASL_data $subject/ASL/Perf*
mkdir  $subject/ASL/ASL_data
$FSLDIR/bin/fslroi $ASL $subject/ASL/ASL_data/M0 0 1
$FSLDIR/bin/fslroi $ASL $subject/ASL/ASL_data/ASL 1 60
$FSLDIR/bin/bet $subject/ASL/ASL_data/M0.nii.gz $subject/ASL/ASL_data/M0_brain.nii.gz -m

$BB_BIN_DIR/bb_pipeline_tools/bb_GDC_half_voxel \
    --workingdir=$subject/ASL/ASL_data/ASL_GDC/ \
    --in=$subject/ASL/ASL_data/M0 \
    --out=$subject/ASL/ASL_data/M0_ud.nii.gz \
    --owarp=$subject/ASL/ASL_data/ASL_ud_warp.nii.gz


# ------- Run the longer processing with the structural ---- #
echo ''
echo '>>>>>>> Running oxasl with structural and distortion correction...'
$FSLDIR/bin/oxford_asl \
       -i $subject/ASL/ASL_data/ASL \
       -c $subject/ASL/ASL_data/M0 \
       -o $subject/ASL/PerfQuantFull \
       --iaf=tc --ibf=rpt --tis=1.7,2.0,2.3,2.6,2.9,3.2 \
       --bolus=1.4 --slicedt=0.0452 --casl \
       --cmethod=voxel \
       --mc --fixbolus \
       --tr=10 \
       --te=14 \
       -s $STRUC \
       --sbrain=${STRUC}_brain \
       --structout \
       --fastsrc=$FASTSRC \
       --warp=$WARP \
       --fmap=$subject/dMRI/dMRI/TOPUP_fout_rad_per_s.nii.gz \
       --fmapmag=$subject/dMRI/dMRI/TOPUP_iout_Tmean  \
       --fmapmagbrain=$subject/dMRI/dMRI/TOPUP_iout_Tmean_bet \
       --gdcwarp=$subject/ASL/ASL_data/ASL_ud_warp.nii.gz \
       --echospacing=0.000559996 \
       --pedir=-y \
       --pvcorr \
       --debug 

echo ''
echo ">>>> $0 done!"

set +x 

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_footer
