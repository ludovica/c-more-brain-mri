#!/bin/sh
#
# Script name: bb_asl_visual_check
#
# Description: Script to visually inspect the output of the ASL processing pipeline
#
# Authors: Thomas Okell, Flora Kennedy McConnell
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

CURDIR=`pwd`
echo Current directory\: $CURDIR

cd PerfQuantFull/fsl* 
echo Check motion...
fsleyes asldata -dr 0 1500 asldata_orig -dr 0 1500

echo Check distortion correction...
fsleyes calib_orig.nii.gz calib_wholehead.nii.gz

echo Check alignment with structural is better after distortion correction...
fsleyes  ../../../T1/T1_unbiased \
    calib_orig.nii.gz -a 50 -dr 900 3000 -cm blue-lightblue \
    calib_wholehead.nii.gz -a 50 -dr 900 3000 -cm red-yellow  

cd ../native_space
echo Native space results...
fsleyes aCBV_calib -dr 0 3 -cm brain_colours_6bluegrn \
    arrival -dr 0.5 3.0 -cm hot \
    pvcorr/perfusion_calib.nii.gz \
    perfusion_calib -dr 0 100 \
    ../fsl*/diffdata_mean.nii.gz -dr -10 50 
    
cd ../struct_space/
echo Structural space results...
fsleyes ../../../T1/T1_unbiased \
    perfusion_calib.nii.gz -cm hot -dr 25 70 -a 50 \
    ../../../T1/T1_fast/T1_brain_pve_1.nii.gz -dr 0.5 0.55 -cm green -a 50

cd ../std_space
echo Standard space results...
fsleyes $FSLDIR/data/standard/MNI152_T1_2mm \
    perfusion_calib.nii.gz -dr 25 70 -cm red-yellow -a 25

cd $CURDIR
echo DONE\!