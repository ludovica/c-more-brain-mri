#!/bin/sh
#
# Script name: bb_IDP_asl
#
# Description: Script to generate the IDPs related to ASL and save them in a similar format to the other UK Biobank IDPs
#
# Authors: Thomas Okell, Flora Kennedy McConnell, Ludovica Griffanti, Fidel Alfaro-Almagro
#
# Copyright 2021 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

origDir=`pwd`
#scriptName=`basename "$0"`
scriptName="bb_IDP_ASL"
direc=$1

cd $direc

result=""

for x in "CBF_gm" "ATT_gm" "CBF_wm" "ATT_wm" "CBF_pvcorr_gm" "ATT_pvcorr_gm" "CBF_pvcorr_wm" "ATT_pvcorr_wm" ; do
    f="ASL/${x}_mean.txt";
    if [ -f $f ] ; then
        result="$result `cat $f`"
    else
        result="$result NaN"
    fi
done

mkdir -p IDP_files

echo $result > IDP_files/$scriptName.txt
echo $result
    
cd $origDir
